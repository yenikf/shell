SOURCES=$(wildcard *.cpp)
OBJS=$(SOURCES:%.cpp=%.o)
TARGETS=$(SOURCES:%.cpp=%)

CPPFLAGS=-g -pthread -std=c++14 -Wall -pedantic
LDFLAGS=-g -pthread -lrt

.PHONY: all clean 

all: $(TARGETS)

%.o: %.cpp
	g++ -c $(CPPFLAGS) $^ -o $@
	
$(TARGETS): %: %.o
	g++  $^ -o $@ $(LDFLAGS)

clean:
	rm -rf *.o $(TARGETS)
