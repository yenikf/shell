/*
	Jan Feific | FEI0017 | 24.03.2019
	OSY 2018/19 | tut3 - Projekt 1
	
	Napište jednoduchý interpret příkazového řádku, který bude akceptovat přesměrování "|" a ">". 
 */

#include <cstdio>		// getchar, fprintf
#include <cctype>		// isalpha, isspace
#include <unistd.h>		// exec, fork, pipe
#include <wait.h>		// wait
#include <fcntl.h>		// open
#include <vector>		// vector
#include <string>		// string

using pipe_t = int[2];

/* ----------- REVEAL_POINTERS ----------- */	// shows vector of std::strings as a vector of char pointers
std::vector<char *> reveal_pointers (std::vector<std::string> const& v)
{
	std::vector<char *> ptrs;
	for (std::string const& s : v) {
		ptrs.push_back (const_cast<char *>(s.c_str ()));
	}
	ptrs.push_back (NULL);
	return ptrs;
}

/* ----------- READ_FILENAME ----------- */		// reads the filename we want to redirect into
std::string read_filename ()
{
	std::string name;
	int ch;
	while (true) {
		ch = getchar ();
		if (isalnum (ch) || ch == '.' || ch == '-' || ch == '_') {	// allowed filename characters
			name += ch;
		} else {								// other characters end the parsing
			if (!name.empty ()) {				// exit reading only when the name is not empty
				ungetc (ch, stdin);				// if ch was a newline character, we need it
				break;
			}
		}
	}
	return name;
}

/* ----------- PARSE_INPUT ----------- */		// transforms the input into separate tokens
std::vector<std::vector<std::string>> parse_input (std::string& outfile)
{
	std::vector<std::vector<std::string>> line;	// line is 'ls -lA | head -5', for example
	std::vector<std::string> command;			// command is 'ls -lA' or 'head -5'
	std::string token;							// token is 'ls' or '-lA', etc.
	int ch;
	while (true) {
		ch = getchar ();
		if (ch == '|') {						// pipe character separates both tokens and commands
			if (!token.empty ()) {				// if there is a token, add it to the command
				command.push_back (token);
				token.clear ();
			}
			if (command.empty ()) { 			// empty command means we must terminate
				fprintf (stderr, "error: empty command next to a pipe\n");
				exit (1);
			}
			line.push_back (command);			// add the command to the line
			command.clear ();
		} else if (ch == '>') {					// gt-arrow means redirection to a file will follow
			if (!token.empty ()) {				// if there is a token, add it to the command
				command.push_back (token);
				token.clear ();
			}
			if (command.empty ()) { 			// empty command means we must terminate
				fprintf (stderr, "error: empty command redirecting to a file\n");
				exit (1);
			}
			line.push_back (command);			// add the command to the line
			command.clear ();
			outfile = read_filename ();			// read the filename and exit
			while ((ch = getchar ()) != '\n') {}	// dump the rest of input
			break;
		} else if (isgraph (ch)) {				// printable character is part of the command token
			token += ch;
		} else if (isspace (ch)) {				// whitespace separates tokens
			if (!token.empty ()) {
				command.push_back (token);
				token.clear ();
			}
		}
		if (ch == '\n') {						// newline separates lines
			if (!command.empty ()) {			// don't save empty commands
				line.push_back (command);
				command.clear();
			}
			if (line.size () == 1 && line[0].size () == 1 && line[0][0] == "exit()") {	// magic word for quit
				exit (0);
			}
			break;								// line is parsed, we can exit the function
		}
	}
	return line;
}

/* ----------- PROCESS_INPUT ----------- */		// processes the input line
void process_input (std::vector<std::vector<std::string>> const& input, std::string const& outfile)
{
	std::vector<pipe_t> pipes (input.size () > 1 ? input.size () - 1 : 0);	// one less pipes than processes
	for (size_t i = 0; i < input.size (); ++i) {
		if (i < pipes.size ()) {				// create pipe
			pipe (pipes[i]);
		}
		if (fork () == 0) {						// child process
			if (i < pipes.size ()) {			// last process doesn't need write pipe
				dup2 (pipes[i][1], 1);
				close (pipes[i][0]);
			}
			if ((i == pipes.size ()) && (!outfile.empty ())) {	// if there is a file redirection
				pid_t fname = open (outfile.c_str (), O_CREAT | O_TRUNC | O_RDWR, 0644);
				dup2 (fname, 1);
			}
			if (i > 0) {						// first process doesn't need read pipe
				dup2 (pipes[i-1][0], 0);
				close (pipes[i-1][1]);
			}
			execvp (input[i][0].c_str (), reveal_pointers (input[i]).data ());
		}										// parent process
		
		if (i > 0) {							// close previous pipe
			close (pipes[i-1][0]);
			close (pipes[i-1][1]);
		}
	}
	while (wait (NULL) > 0) {}					// wait for child processes to end
	return;
}

/* ----------- MAIN ----------- */
int main ()
{
	while (true) {
		printf (">> ");
		std::string output_file;
		auto input = parse_input (output_file);
		process_input (input, output_file);
	}
	return 0;
}

